package com.linxo.gallery.albums

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.*
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.linxo.gallery.R
import com.linxo.gallery.gallery.GalleryActivity
import com.linxo.gallery.models.Album
import com.linxo.gallery.theme.MyApplicationTheme
import dagger.hilt.android.AndroidEntryPoint

const val ARG_ALBUM = "argalbum"

@ExperimentalFoundationApi
@ExperimentalAnimationApi
@AndroidEntryPoint
class AlbumsActivity : ComponentActivity() {

    private val viewModel: AlbumsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.onInit()

        setContent {
            MyApplicationTheme {
                AlbumsView()
            }
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun AlbumsView() {
        val state by viewModel.state.collectAsState()
        val isErrorAfterLoaded by viewModel.isErrorAfterLoaded.collectAsState()

        Scaffold(
            topBar = {
                TopAppBar(title = { Text(text = title.toString()) },
                    actions = {
                        IconButton(onClick = { viewModel.refresh()}) {
                            Icon(Icons.Filled.Refresh,getString(R.string.refresh))
                        }
                    },
                    backgroundColor = Color.Transparent,
                    elevation = 0.dp
                )
            },
            content = {
                Column(
                    modifier = Modifier
                        .fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    when (state) {
                        is AlbumsViewModel.State.LOADING ->
                            CircularProgressIndicator()
                        is AlbumsViewModel.State.LOADED -> {
                            AlbumsList((state as AlbumsViewModel.State.LOADED).albums)
                        }
                        is AlbumsViewModel.State.ERROR ->
                            ErrorView()
                    }
                }

                ErrorSnackbar(isErrorAfterLoaded)
            })
    }

    @Composable
    fun AlbumsList(albums: List<Album>) {
        val isRefreshing by viewModel.isRefreshing.collectAsState()

        SwipeRefresh(
            state = rememberSwipeRefreshState(isRefreshing),
            onRefresh = { viewModel.refresh() },
        ) {
            LazyColumn(
                contentPadding = PaddingValues(horizontal = 16.dp, vertical = 16.dp),
                verticalArrangement = Arrangement.spacedBy(16.dp))
            {
                items(albums.size) { index ->
                    Album(albums[index])
                }
            }
        }
    }

    @Composable
    fun Album(album: Album) {
        Column(
            modifier = Modifier
                .clickable {
                    val intent = Intent(this, GalleryActivity::class.java).apply {
                        putExtra(ARG_ALBUM, album)
                    }
                    startActivity(intent)
                }
                .fillMaxWidth()
        ) {
            Text(text = album.title)
            album.user?.let {
                Text(text = it.name, color = Color.Gray, fontSize = 12.sp)
            }
        }
    }

    @Composable
    fun ErrorView() {
        Image(
            painter = painterResource(R.drawable.error),
            contentDescription = "erreur",
            modifier = Modifier
                .fillMaxWidth()
                .padding(end = 25.dp)
                .height(250.dp)
        )
        Text(text = "Error")
        Button(onClick = {viewModel.refresh()},
            modifier = Modifier.padding(16.dp)) {
            Text(text = getString(R.string.refresh))
        }
    }

    @Composable
    fun ErrorSnackbar(message: String?) {
        AnimatedVisibility(
            enter = fadeIn() + slideInHorizontally(),
            exit = shrinkHorizontally() + fadeOut(),
            visible = !message.isNullOrBlank()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Bottom) {
                Snackbar(
                    action = {
                        Button(onClick = { viewModel.undoSnackbar() }) {
                            Text(getString(R.string.undo))
                        } })
                { Text(text = message ?: "") }
            }
        }
    }
}
