package com.linxo.gallery.albums

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.linxo.gallery.api.ApiRepository
import com.linxo.gallery.models.Album
import com.linxo.gallery.utils.launchSafe
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch


class AlbumsViewModel
@ViewModelInject
constructor(private val repository: ApiRepository): ViewModel() {

    sealed class State
    {
        object LOADING : State()
        data class LOADED(val albums: List<Album>): State()
        data class ERROR(val message: String?): State()
    }

    private val _state = MutableStateFlow<State>(State.LOADING)
    val state: StateFlow<State>
        get() = _state.asStateFlow()

    private val _isErrorAfterLoaded = MutableStateFlow<String?>(null)
    val isErrorAfterLoaded: StateFlow<String?>
        get() = _isErrorAfterLoaded.asStateFlow()

    private val _isRefreshing = MutableStateFlow(false)
    val isRefreshing: StateFlow<Boolean>
        get() = _isRefreshing.asStateFlow()

    private var initialized = false

    fun onInit(){
        if(initialized) return
        loadAlbums()
        initialized = true
    }

    fun refresh() {
        _isRefreshing.value = true
        loadAlbums()
        _isRefreshing.value = false
    }

    private fun loadAlbums(){
        viewModelScope.launchSafe(::handleExceptions) {
            val users = repository.getUsers()
            val albums = repository.getAlbums()

            albums.forEach { album ->
                users.find { album.userId == it.id }?.let {
                    album.user = it
                }
            }

            _state.value = State.LOADED(albums.sortedBy { it.title })
        }
    }

    fun undoSnackbar() {
        _isErrorAfterLoaded.value = null
    }

    private fun handleExceptions(th: Throwable)
    {
        if(_state.value is State.LOADING)
            _state.value = State.ERROR("erreur")
        else if(_state.value is State.LOADED) {
            viewModelScope.launch {
                _isErrorAfterLoaded.value = "erreur"
            }
        }
    }
}