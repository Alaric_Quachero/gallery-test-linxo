package com.linxo.gallery.utils

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun CoroutineScope.launchSafe(onError: (Throwable) -> Unit = {}, block: suspend CoroutineScope.() -> Unit)
{
    val handler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
        onError(throwable)
    }

    launch(handler) { block() }
}