package com.linxo.gallery.gallery

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.*
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import com.google.accompanist.coil.rememberCoilPainter
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.linxo.gallery.R
import com.linxo.gallery.albums.ARG_ALBUM
import com.linxo.gallery.models.Photo
import com.linxo.gallery.models.SamplePhotoProvider
import com.linxo.gallery.models.SamplePhotosProvider
import com.linxo.gallery.theme.MyApplicationTheme
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalFoundationApi
@ExperimentalAnimationApi
@AndroidEntryPoint
class GalleryActivity : ComponentActivity() {

    private val viewModel: GalleryViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.album = intent.getParcelableExtra(ARG_ALBUM)

        viewModel.onInit()

        setContent {
            MyApplicationTheme {
                GalleryView()
            }
        }
    }

    @Preview(showBackground = true)
    @Composable
    fun GalleryView() {
        val state by viewModel.state.collectAsState()
        val isErrorAfterLoaded by viewModel.isErrorAfterLoaded.collectAsState()

        Scaffold(
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = "$title: ${viewModel.album?.title}",
                            maxLines = 2,
                            overflow = TextOverflow.Ellipsis
                        )},
                    navigationIcon = {
                        IconButton(onClick = { onBackPressed() }) {
                            Icon(Icons.Filled.ArrowBack, getString(R.string.back_arrow))
                        }
                    },
                    actions = {
                        IconButton(onClick = { viewModel.refresh() }) {
                            Icon(Icons.Filled.Refresh, getString(R.string.refresh))
                        }
                    },
                    backgroundColor = Color.Transparent,
                    elevation = 0.dp
                )},
            content = {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    when (state) {
                        is GalleryViewModel.State.LOADING ->
                            CircularProgressIndicator()
                        is GalleryViewModel.State.LOADED ->
                            PhotosList((state as GalleryViewModel.State.LOADED).photos)
                        is GalleryViewModel.State.ERROR ->
                            ErrorView()
                    }
                }

                ErrorSnackbar(isErrorAfterLoaded)
            })
    }

    @Preview
    @Composable
    fun PhotosList(@PreviewParameter(SamplePhotosProvider::class) photos: List<Photo>) {
        val isRefreshing by viewModel.isRefreshing.collectAsState()

        SwipeRefresh(
            state = rememberSwipeRefreshState(isRefreshing),
            onRefresh = { viewModel.refresh() }
        ) {
            LazyVerticalGrid(cells = GridCells.Adaptive(minSize = 128.dp)) {
                items(photos.size) { Photo(photos[it]) }
            }
        }
    }

    @Preview
    @Composable
    fun Photo(@PreviewParameter(SamplePhotoProvider::class) photo: Photo) {
        Column {
            Image(
                painter = rememberCoilPainter(
                    request = photo.url,
                    fadeIn = true,
                    previewPlaceholder = R.drawable.error),
                contentDescription = photo.title,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(144.dp)
                    .padding(1.dp)
            )
        }
    }

    @Composable
    fun ErrorView() {
        Image(
            painter = painterResource(R.drawable.error),
            contentDescription = "erreur",
            modifier = Modifier
                .fillMaxWidth()
                .padding(end = 25.dp)
                .height(250.dp)
        )
        Text(text = "Error")
        Button(onClick = {viewModel.refresh()},
            modifier = Modifier.padding(16.dp)) {
            Text(text = getString(R.string.refresh))
        }
    }

    @Composable
    fun ErrorSnackbar(message: String?) {
        AnimatedVisibility(
            enter = fadeIn() + slideInHorizontally(),
            exit = shrinkHorizontally() + fadeOut(),
            visible = !message.isNullOrBlank()
        ) {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Bottom) {
                Snackbar(
                    action = {
                        Button(onClick = { viewModel.undoSnackbar() }) {
                            Text(getString(R.string.undo))
                        }})
                { Text(text = message ?: "") }
            }
        }
    }
}
