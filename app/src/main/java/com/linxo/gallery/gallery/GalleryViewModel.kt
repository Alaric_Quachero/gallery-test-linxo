package com.linxo.gallery.gallery

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.linxo.gallery.api.ApiRepository
import com.linxo.gallery.models.Album
import com.linxo.gallery.models.Photo
import com.linxo.gallery.utils.launchSafe
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GalleryViewModel
    @ViewModelInject
    constructor(private val repository: ApiRepository): ViewModel() {

    var album: Album? = null

    sealed class State
    {
        object LOADING : State()
        data class LOADED(val photos: List<Photo>): State()
        data class ERROR(val message: String?): State()
    }

    private val _state = MutableStateFlow<State>(State.LOADING)
    val state: StateFlow<State>
        get() = _state.asStateFlow()

    private val _isErrorAfterLoaded = MutableStateFlow<String?>(null)
    val isErrorAfterLoaded: StateFlow<String?>
        get() = _isErrorAfterLoaded.asStateFlow()

    private val _isRefreshing = MutableStateFlow(false)
    val isRefreshing: StateFlow<Boolean>
        get() = _isRefreshing.asStateFlow()

    private var initialized = false

    fun onInit(){
        if(initialized) return
        loadPhotos()
        initialized = true
    }

    fun refresh() {
        _isRefreshing.value = true
        loadPhotos()
        _isRefreshing.value = false
    }

    private fun loadPhotos(){
        viewModelScope.launchSafe(::handleExceptions) {
            val photos = repository.getPhotos(album?.id ?: -1)

            _state.value = State.LOADED(photos)
        }
    }

    fun undoSnackbar() {
        _isErrorAfterLoaded.value = null
    }

    private fun handleExceptions(th: Throwable)
    {
        if(_state.value is State.LOADING)
            _state.value = State.ERROR("erreur")
        else if(_state.value is State.LOADED) {
            viewModelScope.launch {
                _isErrorAfterLoaded.value = "erreur"
            }
        }
    }
}