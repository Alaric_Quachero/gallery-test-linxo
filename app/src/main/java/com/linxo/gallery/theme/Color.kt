package com.linxo.gallery.theme

import androidx.compose.ui.graphics.Color

val purple = Color(0xFFBE26FF)
val purple2 = Color(0xFF9339FF)
val blue = Color(0xFF3362FF)