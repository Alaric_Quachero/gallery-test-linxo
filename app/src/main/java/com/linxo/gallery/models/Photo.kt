package com.linxo.gallery.models

import androidx.compose.ui.tooling.preview.PreviewParameterProvider

data class Photo(
    val url: String,
    val title: String
)

class SamplePhotoProvider: PreviewParameterProvider<Photo> {
    override val values = sequenceOf(Photo("Jens","ee"),Photo("Jim","ee"))
    override val count: Int = values.count()
}

class SamplePhotosProvider: PreviewParameterProvider<List<Photo>> {
    override val values = sequenceOf(listOf(Photo("Jens","ee"),Photo("Jim","ee")))
    override val count: Int = values.count()
}