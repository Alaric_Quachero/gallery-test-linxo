package com.linxo.gallery.api

import com.linxo.gallery.models.Album
import com.linxo.gallery.models.Photo
import com.linxo.gallery.models.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("albums")
    suspend fun getAlbums(): Response<List<Album>>

    @GET("users")
    suspend fun getUsers(): Response<List<User>>

    @GET("albums/{albumId}/photos")
    suspend fun getPhotos(@Path("albumId") albumId : Int): Response<List<Photo>>
}