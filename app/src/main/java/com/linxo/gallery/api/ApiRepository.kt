package com.linxo.gallery.api

import com.linxo.gallery.models.Album
import com.linxo.gallery.models.Photo
import com.linxo.gallery.models.User

interface ApiRepository {

    suspend fun getAlbums(): List<Album>

    suspend fun getUsers(): List<User>

    suspend fun getPhotos(albumId : Int): List<Photo>
}