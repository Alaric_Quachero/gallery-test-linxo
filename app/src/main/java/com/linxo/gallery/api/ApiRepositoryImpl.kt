package com.linxo.gallery.api

import com.linxo.gallery.models.Album
import com.linxo.gallery.models.Photo
import com.linxo.gallery.models.User

class ApiRepositoryImpl(private val remote: ApiService): ApiRepository{

    override suspend fun getAlbums(): List<Album> {
        return remote.getAlbums().body() ?: emptyList()
    }

    override suspend fun getUsers(): List<User> {
        return remote.getUsers().body() ?: emptyList()
    }

    override suspend fun getPhotos(albumId: Int): List<Photo> {
        return remote.getPhotos(albumId).body() ?: emptyList()
    }
}