package com.linxo.gallery.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetworkSpecificModule
{
    @Singleton
    @Provides
    @ApiUrl
    fun provideApiUrl(): String {
        return "https://jsonplaceholder.typicode.com/"
    }

    @Singleton
    @Provides
    @OkHttp
    fun provideOkHttpClientService(): OkHttpClient {
        return OkHttpClient
            .Builder()
            .addNetworkInterceptor(StethoInterceptor())
            .build()
    }
}