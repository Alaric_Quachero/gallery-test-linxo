-------------------------- Organisation -------------------------- 

J'ai passé une 15aine d'heures sur le projet, réparti sur plusieurs jours (j'ai avancé selon mon temps libre et mes envies). Je me suis organisé en tâches, écrites sur un document, que j'ai validé petit à petit.
J'aurais pu utiliser un Kanban board comme Trello, mais étant seul sur le projet, une simple liste m'a paru suffisante. 

J'ai utilisé git (https://bitbucket.org/Alaric_Quachero/gallery-test-linxo.git), afin de garder un historique de mon avancée et afin de pouvoir revenir à une version antérieur en cas de problème. 
Je me suis contenté d'une branche develop et master, j'aurais pu travailler en gitflow, mais j'ai estimé que ce n'était pas nécessaire étant donné que le projet est un test, et que je suis seul dessus.


-------------------------- Libraries -------------------------- 

J'ai profité du projet afin d'essayer de nouvelles choses, de mettre en place des outils que je n'avais pas eu l'occasion de mettre en place auparavant et d'améliorer ma compréhension de certaines librairies.
J'ai donc travaillé avec Android Studio Bumblebee | 2021.1.1 Canary 1 (Build #AI-203.7717.56.2111.7361063, built on May 14, 2021).

J'ai utilisé les librairies suivantes:

- Retrofit et Okhhtp: Pour gérer les requêtes API.

- Hilt Dagger: Pour faire des injections de dépendances, notamment pour pouvoir injecter le repository pour utiliser les requêtes API dans le viewmodel.

- Stetho: Pour intercepter les requêtes API, afin de vérifier qu'elles sont bien appelées comme il faut. On peut ainsi voir les requêtes via Chrome (buggué depuis peu) ou via Microsoft edge, en utilisant l'url chrome://inspect/#devices.

- Compose: On en a parlé lors de l'entretien, et j'étais curieux de voir de quoi il s'agissait concrètement. J'ai donc profité du test pour commencé à me servir de cette librairie.

- Accompanist: Pour faciliter l'utilisation de Compose, notamment avec Accompanist Coil pour charger les images par url, et Accompanist SwipeRefresh, pour permettre un swipe refresh sur les listes.


-------------------------- Particular practices -------------------------- 

J'ai structuré le projet en MVVM. Cela me semble une bonne façon de garder un projet clair et de s'y repérer aisément.
Afin de communiquer entre les ViewModels et les Activities (vues), j'ai utilisé des StateFlow, ça permet de changer la vue dès qu'un changement de valeur est effectué côté ViewModel.

J'ai aussi rapidement mis en place un système de Flavors, afin de gérer un environnement QA (environnement de test. Le nom n'est pas forcément adapté, mais c'est comme ça qu'on appelait ça dans mon ancienne entreprise), et prod.
Cela me permet notamment d'utiliser Stetho (interception de requête) dans l'environnement QA et pas dans l'environnement prod. 
De même, je peux charger une url pour l'API différente entre la QA et la prod (j'ai fait comme si il y avait une API de test et une API de prod, c'est pas très pertinent pour ce projet, mais je tenais à essayer de mettre en place ça).


-------------------------- Améliorations -------------------------- 

J'ai noté plusieurs améliorations que j'aimerais faire par rapport à mon rendu. J'essaierais peut être d'en faire quelques unes après le rendu, afin de progresser sur certains points.

Je pourrais factoriser certaines parties de mon code. Comme on peut le voir, beaucoup de choses sont communes à la GalleryActivity et l'AlbumActivity (topbar, vue erreur, snackbar, refresh, ...). 
De même pour l'AlbumsViewModel et la GalleryViewModel (on a toujours un init, un load, un refresh, une fonction pour undo la snackbar, ...).
Je pense qu'il y a moyen de synthétiser ça en créant une classe parent pour les activités, et une classe parent pour les viewmodels, qui s'occuperaient de gérer ces parties la, et ou on spécifierait plus que la requête à charger, les
messages à afficher dans les erreurs, l'agence du contenu des listes à afficher, ...

Il faudrait que j'ajoute des tests:
- Pour le Service: tester que la bonne route est appelée, et que ça me renvoie les bonnes valeurs.
- Pour le Repository: vérifier qu'il a bien appelé ce qu'il faut (ça n'a pas grand intêret ici, sauf si, comme je vais en parler plus tard, on ajoute une partie base de donnée locale).
- Pour les ViewModels: vérifier qu'en appelant les fonctions d'init, de refresh, ... les viewModels nous renvoient le bonne état, avec les bonnes données associées.

De même, avec les tests, il serait bien que le projet est une solution d'intégration continu (Jenkins, Bitrise, ...). J'aimerais en mettre une en place après le rendu, par curiosité, étant donné que j'ai travaillé sur des projets
en aillant déjà une en place, mais que je n'ai jamais eu l'occasion d'en configurer une par moi même.

Il faudrait que je gère les différents messages d'erreur: Actuellement, la snackbar et la vue erreur renvoie le même message peut importe l'erreur. Il faudrait que ces éléments renvoient des messages différents selon le status de
l'erreur ("Notre serveur rencontre des problèmes actuellement, réessayez plus tard", "Vous n'êtes pas connecté à internet", ...).

Certains éléments visuels sont à améliorer: La snackbar est celle par défaut et s'intégre mal à l'application, quand on clique sur le refresh en haut à droite, il serait bien d'avoir une petite animation ou il tourne sur lui même
afin de montrer qu'il charge les données, la status bar et la nav bar en thème sombre, ...

J'aimerais aussi faire fonctionner les previews avec Android Studio Canary. En effet, l'IDE propose une preview des éléments Compose, mais j'ai des petits soucis pour faire fonctionner cette dernière par rapport au ViewModel. J'ai pas
pris le temps de trouver une solution, et je pense chercher ça après le rendu.

D'autres choses peuvent être améliorer: 
- Rajout d'une BDD locale: Pour que quelqu'un ayant déjà chargé l'application et la réouvrant en étant hors ligne puisse consulter les données qu'il a déjà chargé auparavant.
- Fonction recherche sur la liste album: Afin de trouver rapidement un album avec son nom, ou le nom de son auteur. On chargerait la liste complète et on appliquerait un filtre en direct avec les flows pour montrer que les albums
possédant le texte recherché dans leur nom ou dans le nom de leur auteur.
- Corriger le bug du clic sur deux albums différents: Quand on clique sur deux albums en même temps, il arrive que ça charge les deux pages, du coup, quand on navigue en arrière, on tombe sur le deuxième album chargé. Pour se faire,
il faudrait soit rendre l'activité unique (on ne pourrait plus charger deux activités album l'une sur l'autre), soit empêcher le clic pendant un petit laps de temps après un premier clic.
- Rendre les images de l'album cliquables: Afin de les voir en grand, de pouvoir zoomer dessus, de pouvoir slider d'une image à l'autre, de pouvoir partager l'image, l'enregistrer, ... comme sur l'application galerie des smartphones.
- On pourrait imaginer tout une série de fonctions types supprimer un album, supprimer une photo, ajouter un album, ajouter une photo, signaler un album, signaler une photo, ...


-------------------------- Petit mot de la fin -------------------------- 

Je vous remercie de m'accorder du temps afin de lire mon code et de me faire des retours. J'ai, pour ma part, pris du plaisir à faire le projet en essayant de nouvelles choses, et je serais ravi d'avoir votre avis afin d'améliorer mon
code et de progresser dans mes compétences.

